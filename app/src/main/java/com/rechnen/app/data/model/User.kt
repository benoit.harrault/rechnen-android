/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.rechnen.app.data.modelparts.InputConfiguration
import com.rechnen.app.data.modelparts.TaskDifficulty

@Entity(tableName = "user")
data class User(
        @PrimaryKey(autoGenerate = true)
        val id: Int,
        val name: String,
        @ColumnInfo(name = "last_use")
        val lastUse: Long?,
        @ColumnInfo(name = "last_block_count")
        val lastBlockCount: Int,
        val difficulty: TaskDifficulty,
        @ColumnInfo(name = "play_time")
        val playTime: Long,
        @ColumnInfo(name = "solved_tasks")
        val solvedTasks: Long,
        @ColumnInfo(name = "input_configuration")
        val inputConfiguration: InputConfiguration = InputConfiguration.default
)