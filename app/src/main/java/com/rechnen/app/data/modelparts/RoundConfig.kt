/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data.modelparts

import android.util.JsonReader
import android.util.JsonWriter

data class RoundConfig(
        val maximalTimePerRound: Int = 60 * 1000,
        val maximalMistakesPerRound: Int = 2,
        val tasksPerRound: Int = 10,
        val reAskWrongInNextRound: Int = 1,
        val reAskRightInNextRound: Int = 3
) {
    companion object {
        private const val MAX_TIME_PER_ROUND = "maxTimePerRound"
        private const val MAX_MISTAKES_PER_ROUND = "maxMistakesPerRound"
        private const val TASKS_PER_ROUND = "tasksPerRound"
        private const val RE_ASK_WRONG_IN_NEXT_ROUND = "reAskWrongInNextRound"
        private const val RE_ASK_RIGHT_IN_NEXT_ROUND = "reAskRightInNextRound"

        fun parse(reader: JsonReader): RoundConfig {
            var maximalTimePerRound: Int? = null
            var maximalMistakesPerRound: Int? = null
            var tasksPerRound: Int? = null
            var reAskWrongInNextRound: Int? = null
            var reAskRightInNextRound: Int? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    MAX_TIME_PER_ROUND -> maximalTimePerRound = reader.nextInt()
                    MAX_MISTAKES_PER_ROUND -> maximalMistakesPerRound = reader.nextInt()
                    TASKS_PER_ROUND -> tasksPerRound = reader.nextInt()
                    RE_ASK_WRONG_IN_NEXT_ROUND -> reAskWrongInNextRound = reader.nextInt()
                    RE_ASK_RIGHT_IN_NEXT_ROUND -> reAskRightInNextRound = reader.nextInt()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return RoundConfig(
                    maximalTimePerRound = maximalTimePerRound!!,
                    maximalMistakesPerRound = maximalMistakesPerRound!!,
                    tasksPerRound = tasksPerRound!!,
                    reAskWrongInNextRound = reAskWrongInNextRound!!,
                    reAskRightInNextRound = reAskRightInNextRound!!
            )
        }
    }

    init {
        if (maximalTimePerRound <= 0 || maximalMistakesPerRound < 0 || tasksPerRound <= 0 || reAskWrongInNextRound < 0 || reAskRightInNextRound < 0) {
            throw IllegalArgumentException()
        }

        if (reAskRightInNextRound > tasksPerRound || reAskWrongInNextRound > maximalMistakesPerRound || maximalMistakesPerRound > tasksPerRound) {
            throw IllegalArgumentException()
        }
    }

    fun serialize(writer: JsonWriter) {
        writer.beginObject()

        writer.name(MAX_TIME_PER_ROUND).value(maximalTimePerRound)
        writer.name(MAX_MISTAKES_PER_ROUND).value(maximalMistakesPerRound)
        writer.name(TASKS_PER_ROUND).value(tasksPerRound)
        writer.name(RE_ASK_WRONG_IN_NEXT_ROUND).value(reAskWrongInNextRound)
        writer.name(RE_ASK_RIGHT_IN_NEXT_ROUND).value(reAskRightInNextRound)

        writer.endObject()
    }
}