/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.rechnen.app.data.converter.InputConfigurationConverter
import com.rechnen.app.data.converter.TaskConverter
import com.rechnen.app.data.converter.TaskDifficultyConverter
import com.rechnen.app.data.dao.SavedTaskDao
import com.rechnen.app.data.dao.StatisticTaskResultDao
import com.rechnen.app.data.dao.UserDao
import com.rechnen.app.data.model.SavedTask
import com.rechnen.app.data.model.StatisticTaskResult
import com.rechnen.app.data.model.User

@Database(
        version = 6,
        entities = [
            User::class,
            SavedTask::class,
            StatisticTaskResult::class
        ]
)
@TypeConverters(
        TaskDifficultyConverter::class,
        TaskConverter::class,
        InputConfigurationConverter::class
)
abstract class Database: RoomDatabase() {
    abstract fun user(): UserDao
    abstract fun savedTask(): SavedTaskDao
    abstract fun statisticTaskResult(): StatisticTaskResultDao
}