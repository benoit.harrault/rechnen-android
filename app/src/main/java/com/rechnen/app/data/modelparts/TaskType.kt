/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data.modelparts

enum class TaskType {
    Addition,
    Subtraction,
    Multiplication,
    Factorization,
    Division
}

object TaskTypes {
    private const val ADDITION = "addition"
    private const val SUBTRACTION = "subtraction"
    private const val MULTIPLICATION = "multiplication"
    private const val FACTORIZATION = "factorization"
    private const val DIVISION = "division"

    fun toString(type: TaskType): String = when (type) {
        TaskType.Addition -> ADDITION
        TaskType.Subtraction -> SUBTRACTION
        TaskType.Multiplication -> MULTIPLICATION
        TaskType.Factorization -> FACTORIZATION
        TaskType.Division -> DIVISION
    }

    fun fromString(type: String): TaskType = when (type) {
        ADDITION -> TaskType.Addition
        SUBTRACTION -> TaskType.Subtraction
        MULTIPLICATION -> TaskType.Multiplication
        FACTORIZATION -> TaskType.Factorization
        DIVISION -> TaskType.Division
        else -> throw IllegalArgumentException()
    }
}