/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data.modelparts

import android.util.JsonReader
import android.util.JsonWriter

// dividend / divisor = quotient
data class DivisionConfig(
        val enable: Boolean = false,
        val withRemainder: Boolean = false,
        val maxDividend: Int = 100,
        val maxDivisor: Int = 10
) {
    companion object {
        private const val ENABLE = "enable"
        private const val WITH_REMAINDER = "withRemainder"
        private const val MAX_DIVIDEND = "maxDividend"
        private const val MAX_DIVISOR = "maxDivisor"

        val MAX_DIVIDEND_SUGGESTIONS = listOf(10, 20, 50, 100, 500, 1000, 10000)
        val MAX_DIVISOR_SUGGESTIONS = listOf(5, 10, 20, 50, 100)    // must be a subset of MAX_DIVIDEND_SUGGESTIONS

        fun parse(reader: JsonReader): DivisionConfig {
            var enable: Boolean? = null
            var withRemainder: Boolean? = null
            var maxDividend: Int? = null
            var maxDivisor: Int? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    ENABLE -> enable = reader.nextBoolean()
                    WITH_REMAINDER -> withRemainder = reader.nextBoolean()
                    MAX_DIVIDEND -> maxDividend = reader.nextInt()
                    MAX_DIVISOR -> maxDivisor = reader.nextInt()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return DivisionConfig(
                    enable = enable!!,
                    withRemainder = withRemainder!!,
                    maxDividend = maxDividend!!,
                    maxDivisor = maxDivisor!!
            )
        }
    }

    init {
        if (maxDividend < maxDivisor || maxDividend <= 0 || maxDivisor <= 0) {
            throw IllegalArgumentException()
        }
    }

    fun serialize(writer: JsonWriter) {
        writer.beginObject()

        writer.name(ENABLE).value(enable)
        writer.name(WITH_REMAINDER).value(withRemainder)
        writer.name(MAX_DIVIDEND).value(maxDividend)
        writer.name(MAX_DIVISOR).value(maxDivisor)

        writer.endObject()
    }
}