/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.training.statistic

import android.content.Context
import com.rechnen.app.data.modelparts.Task
import com.rechnen.app.data.modelparts.TaskType
import com.rechnen.app.ui.training.*

sealed class TrainingStatisticListItem {
    companion object {
        fun from(input: List<TrainingStatisticItemByParameter>): List<TrainingStatisticListItem> {
            val result = mutableListOf<TrainingStatisticListItem>()

            val maxAvgDuration = input.filter { it.parameter == TrainingStatisticItemParameter.AverageDurationPerCorrectTask }.map { inputRow ->
                inputRow.values.values.max()!!
            }.max() ?: 0

            input.forEach { inputRow ->
                result.add(
                        TypeHeader(
                                taskType = inputRow.taskType,
                                parameter = inputRow.parameter,
                                change = inputRow.change
                        )
                )

                inputRow.values.entries.sortedBy { TrainingStatisticItemSourceSorting.rank(it.key) }.forEach { (key, value) ->
                    result.add(
                            DataRow(
                                    parameter = inputRow.parameter,
                                    source = key,
                                    value = value,
                                    max = when (inputRow.parameter) {
                                        TrainingStatisticItemParameter.PercentageOfCorrectlySolvedTasks -> 100
                                        TrainingStatisticItemParameter.AverageDurationPerCorrectTask -> maxAvgDuration
                                    }
                            )
                    )
                }
            }

            return result
        }
    }

    data class TypeHeader(val taskType: TaskType, val parameter: TrainingStatisticItemParameter, val change: TrainingStatisticItemChange?): TrainingStatisticListItem()
    data class DataRow(val parameter: TrainingStatisticItemParameter, val source: TrainingStatisticItemSource, val value: Int, val max: Int): TrainingStatisticListItem()
    data class ResultInfoHeadline(val text: (Context) -> String): TrainingStatisticListItem()
    data class ResultFooterText(val textRessourceId: Int): TrainingStatisticListItem()
    data class TaskInfo(val task: String, val duration: Long, val input: String, val correct: Boolean): TrainingStatisticListItem()
}