/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.user

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.rechnen.app.R
import com.rechnen.app.Threads
import com.rechnen.app.data.AppDatabase
import com.rechnen.app.data.modelparts.*
import com.rechnen.app.databinding.DifficultyDialogBinding
import com.rechnen.app.extension.*
import com.rechnen.app.ui.view.EnableOperatorListener
import com.rechnen.app.ui.view.EnableOperatorStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class EditUserDifficultyDialog: BottomSheetDialogFragment() {
    companion object {
        private const val DIALOG_TAG = "EditUserDifficultyDialog"
        private const val EXTRA_USER_ID = "userId"
        private const val STATE_DIFFICULTY = "difficulty"
        private const val STATE_SCREEN = "screen"

        private const val PAGE_OVERVIEW = 0
        private const val PAGE_MULTIPLICATION = 1
        private const val PAGE_ADDITION = 2
        private const val PAGE_SUBTRACTION = 3
        private const val PAGE_PROFILES = 4
        private const val PAGE_FACTORIZATION = 5
        private const val PAGE_DIVISION = 6
        private const val PAGE_LOADING = 7
        private const val PAGE_GENERAL = 8

        fun newInstance(userId: Int) = EditUserDifficultyDialog().apply {
            arguments = Bundle().apply {
                putInt(EXTRA_USER_ID, userId)
            }
        }
    }

    private var didLoadDifficulty = false
    private var isUpdatingValues = false
    private lateinit var difficulty: TaskDifficulty
    private lateinit var binding: DifficultyDialogBinding

    private fun formatDigitNumber(n: Int) = resources.getQuantityString(R.plurals.difficulty_digit_counter, n, n)

    private fun updateOverview() {
        val withTenTransgression = getString(R.string.suffix_with_ten_transgression)
        val withoutTenTransgression = getString(R.string.suffix_without_ten_transgression)

        binding.profileButton.text = getString(
                TaskDifficultyProfiles.profilesAndLabels.find { it.first == difficulty }?.second
                        ?: R.string.difficulty_profile_custom
        )

        binding.additionButton.text = if (difficulty.addition.enable) {
            getString(
                    R.string.difficulty_operation_addition_enabled,
                    formatDigitNumber(difficulty.addition.digitsOfFirstNumber),
                    formatDigitNumber(difficulty.addition.digitsOfSecondNumber),
                    if (difficulty.addition.digitsOfSecondNumber > 1)
                        ""
                    else if (difficulty.addition.enableTenTransgression)
                        withTenTransgression
                    else
                        withoutTenTransgression
            )
        } else {
            getString(R.string.difficulty_operation_disabled)
        }

        binding.subtractionButton.text = if (difficulty.subtraction.enable) {
            getString(
                    R.string.difficulty_operation_subtraction_enabled,
                    formatDigitNumber(difficulty.subtraction.digitsOfFirstNumber),
                    formatDigitNumber(difficulty.subtraction.digitsOfSecondNumber),
                    if (difficulty.subtraction.allowNegativeResults)
                        getString(R.string.suffix_with_negative_results)
                    else if (difficulty.subtraction.digitsOfSecondNumber > 1)
                        ""
                    else if (difficulty.subtraction.enableTenTransgression)
                        withTenTransgression
                    else
                        withoutTenTransgression
            )
        } else {
            getString(R.string.difficulty_operation_disabled)
        }

        binding.multiplicationButton.text = if (difficulty.multiplication.enable) {
            getString(
                    R.string.difficulty_operation_multiplication_enabled,
                    formatDigitNumber(difficulty.multiplication.digitsOfFirstNumber),
                    formatDigitNumber(difficulty.multiplication.digitsOfSecondNumber)
            )
        } else {
            getString(R.string.difficulty_operation_disabled)
        }

        binding.factorizationButton.text = if (difficulty.factorization.enable) {
            getString(R.string.difficulty_operation_factorization_enabled, difficulty.factorization.smallestNumber, difficulty.factorization.biggestNumber)
        } else {
            getString(R.string.difficulty_operation_disabled)
        }

        binding.divisionButton.text = if (difficulty.division.enable) {
            getString(
                    R.string.difficulty_operation_division_enabled,
                    difficulty.division.maxDividend,
                    difficulty.division.maxDivisor,
                    getString(if (difficulty.division.withRemainder) R.string.suffix_with_remainder else R.string.suffix_without_remainder)
            )
        } else {
            getString(R.string.difficulty_operation_disabled)
        }

        binding.generalButton.text = getString(
                R.string.difficulty_general_summary,
                resources.getQuantityString(R.plurals.difficulty_general_summary_tasks, difficulty.roundConfig.tasksPerRound, difficulty.roundConfig.tasksPerRound),
                resources.getQuantityString(
                        R.plurals.training_result_seconds,
                        difficulty.roundConfig.maximalTimePerRound / 1000,
                        difficulty.roundConfig.maximalTimePerRound / 1000
                ),
                resources.getQuantityString(R.plurals.difficulty_general_summary_mistake, difficulty.roundConfig.maximalMistakesPerRound, difficulty.roundConfig.maximalMistakesPerRound),
                difficulty.roundConfig.reAskWrongInNextRound,
                difficulty.roundConfig.reAskRightInNextRound
        )
    }

    private fun updateMultiplication() {
        val binding = binding.multiplication
        val thisDifficulty = difficulty.multiplication

        binding.enableOperation = thisDifficulty.enable

        isUpdatingValues = true
        binding.enableView.status = getOperatorStatus(difficulty, TaskType.Multiplication)
        binding.digits1.progress = thisDifficulty.digitsOfFirstNumber - 1
        binding.digits2.progress = thisDifficulty.digitsOfSecondNumber - 1
        isUpdatingValues = false

        binding.firstDigitsText = formatDigitNumber(thisDifficulty.digitsOfFirstNumber)
        binding.secondDigitsText = formatDigitNumber(thisDifficulty.digitsOfSecondNumber)
    }

    private fun updateAddition() {
        val binding = binding.addition
        val thisDifficulty = difficulty.addition

        binding.enableOperation = thisDifficulty.enable

        isUpdatingValues = true
        binding.enableView.status = getOperatorStatus(difficulty, TaskType.Addition)
        binding.digits1.progress = thisDifficulty.digitsOfFirstNumber - 1
        binding.digits2.progress = thisDifficulty.digitsOfSecondNumber - 1
        binding.allowAdjustSecondOperandSize = thisDifficulty.enableTenTransgression
        binding.tenTransgressionSwitch.isChecked = thisDifficulty.enableTenTransgression
        isUpdatingValues = false

        binding.firstDigitsText = formatDigitNumber(thisDifficulty.digitsOfFirstNumber)
        binding.secondDigitsText = formatDigitNumber(thisDifficulty.digitsOfSecondNumber)
    }

    private fun updateSubtraction() {
        val binding = binding.subtraction
        val thisDifficulty = difficulty.subtraction

        binding.enableOperation = thisDifficulty.enable

        isUpdatingValues = true
        binding.enableView.status = getOperatorStatus(difficulty, TaskType.Subtraction)
        binding.digits1.progress = thisDifficulty.digitsOfFirstNumber - 1
        binding.digits2.progress = thisDifficulty.digitsOfSecondNumber - 1
        binding.tenTransgressionSwitch.isChecked = thisDifficulty.enableTenTransgression
        binding.negativeResultSwitch.isChecked = thisDifficulty.allowNegativeResults
        binding.allowAdjustSecondOperandSize = thisDifficulty.enableTenTransgression
        binding.allowAdjustNegativeResults = thisDifficulty.enableTenTransgression
        isUpdatingValues = false

        binding.firstDigitsText = formatDigitNumber(thisDifficulty.digitsOfFirstNumber)
        binding.secondDigitsText = formatDigitNumber(thisDifficulty.digitsOfSecondNumber)
    }

    private fun updateFactorization() {
        val binding = binding.factorization
        val thisDifficulty = difficulty.factorization

        binding.enableOperation = thisDifficulty.enable

        isUpdatingValues = true
        binding.enableView.status = getOperatorStatus(difficulty, TaskType.Factorization)

        binding.smallestNumber.minValue = FactorizationConfig.MIN_NUMBER
        binding.smallestNumber.maxValue = thisDifficulty.biggestNumber

        binding.biggestNumber.minValue = thisDifficulty.smallestNumber
        binding.biggestNumber.maxValue = FactorizationConfig.MAX_NUMBER

        binding.smallestNumber.value = thisDifficulty.smallestNumber
        binding.biggestNumber.value = thisDifficulty.biggestNumber

        isUpdatingValues = false
    }

    private fun updateDivision() {
        val binding = binding.division
        val thisDifficulty = difficulty.division

        binding.enableOperation = thisDifficulty.enable

        isUpdatingValues = true
        binding.enableView.status = getOperatorStatus(difficulty, TaskType.Division)

        binding.allowRemainderSwitch.isChecked = thisDifficulty.withRemainder

        binding.maxDividend = thisDifficulty.maxDividend
        binding.maxDivisor = thisDifficulty.maxDivisor

        binding.divisorBar.progress = DivisionConfig.MAX_DIVISOR_SUGGESTIONS.seekBarIndexOf(thisDifficulty.maxDivisor)
        binding.dividendBar.progress = DivisionConfig.MAX_DIVIDEND_SUGGESTIONS.seekBarIndexOf(thisDifficulty.maxDividend)

        isUpdatingValues = false
    }

    private fun updateProfiles() {
        binding.profile.easy.isChecked = difficulty == TaskDifficultyProfiles.easy
        binding.profile.medium.isChecked = difficulty == TaskDifficultyProfiles.medium
        binding.profile.hard.isChecked = difficulty == TaskDifficultyProfiles.hard
    }

    private fun updateGeneral() {
        val binding = binding.general
        val config = difficulty.roundConfig

        binding.tasksPerRoundText = resources.getQuantityString(R.plurals.difficulty_tasks_per_block, config.tasksPerRound, config.tasksPerRound)
        binding.timePerRoundText = getString(
                R.string.difficulty_time_per_round,
                resources.getQuantityString(R.plurals.training_result_seconds, config.maximalTimePerRound / 1000, config.maximalTimePerRound / 1000)
        )
        binding.maxWrongTasksText = resources.getQuantityString(R.plurals.difficulty_max_wrong_tasks, config.maximalMistakesPerRound, config.maximalMistakesPerRound)
        binding.takeRightTasksText = resources.getQuantityString(R.plurals.difficulty_take_right_tasks, config.reAskRightInNextRound, config.reAskRightInNextRound)
        binding.takeWrongTasksText = resources.getQuantityString(R.plurals.difficulty_take_wrong_tasks, config.reAskWrongInNextRound, config.reAskWrongInNextRound)

        isUpdatingValues = true

        binding.timePerRound.progress = config.maximalTimePerRound / (config.tasksPerRound * 1000) - 1
        binding.tasksPerRound.progress = config.tasksPerRound / 5 - 1

        binding.maxWrongTasks.max = config.tasksPerRound
        binding.maxWrongTasks.progress = config.maximalMistakesPerRound

        binding.takeWrongTasks.max = config.maximalMistakesPerRound
        binding.takeWrongTasks.progress = config.reAskWrongInNextRound

        binding.takeRightTasks.max = config.tasksPerRound
        binding.takeRightTasks.progress = config.reAskRightInNextRound

        isUpdatingValues = false
    }

    private fun updateAll() {
        updateOverview()
        updateMultiplication()
        updateAddition()
        updateSubtraction()
        updateFactorization()
        updateDivision()
        updateProfiles()
        updateGeneral()
    }

    private fun getOperatorStatus(difficulty: TaskDifficulty, type: TaskType) = if (difficulty.enabledTaskTypes.contains(type))
        if (difficulty.enabledTaskTypes.size == 1) EnableOperatorStatus.LastEnabled else EnableOperatorStatus.Enabled
    else
        EnableOperatorStatus.Disabled

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val database = AppDatabase.with(context!!)
        val userId = arguments!!.getInt(EXTRA_USER_ID)

        binding = DifficultyDialogBinding.inflate(inflater, container, false)

        val oldDifficulty: TaskDifficulty? = savedInstanceState?.getString(STATE_DIFFICULTY)?.useJsonReader { TaskDifficulty.parse(it) }

        if (oldDifficulty != null) {
            difficulty = oldDifficulty
            didLoadDifficulty = true

            updateAll()

            binding.flipper.displayedChild = savedInstanceState.getInt(STATE_SCREEN, PAGE_OVERVIEW)
        } else {
            binding.flipper.displayedChild = PAGE_LOADING

            GlobalScope.launch (Dispatchers.Main) {
                difficulty = database.user().getUserByIdCoroutine(userId)!!.difficulty
                didLoadDifficulty = true

                updateAll()
                binding.flipper.displayedChild = PAGE_OVERVIEW
            }
        }

        binding.profileButton.setOnClickListener { updateProfiles(); binding.flipper.openSubscreen(PAGE_PROFILES) }
        binding.multiplicationButton.setOnClickListener { binding.flipper.openSubscreen(PAGE_MULTIPLICATION) }
        binding.additionButton.setOnClickListener { binding.flipper.openSubscreen(PAGE_ADDITION) }
        binding.subtractionButton.setOnClickListener { binding.flipper.openSubscreen(PAGE_SUBTRACTION) }
        binding.factorizationButton.setOnClickListener { binding.flipper.openSubscreen(PAGE_FACTORIZATION) }
        binding.divisionButton.setOnClickListener { binding.flipper.openSubscreen(PAGE_DIVISION) }
        binding.generalButton.setOnClickListener { binding.flipper.openSubscreen(PAGE_GENERAL) }
        binding.saveButton.setOnClickListener {
            Threads.database.submit {
                database.runInTransaction {
                    val previousDifficulty = database.user().getUserDifficultyByUserIdSync(userId)
                    val newDifficulty = difficulty

                    if (previousDifficulty != newDifficulty) {
                        database.savedTask().deleteUserTasksSync(userId)
                        database.statisticTaskResult().deleteStatisticTaskResultsForUserSync(userId)

                        database.user().updateUserDifficultyConfigSync(userId, newDifficulty)
                    }
                }
            }

            Toast.makeText(context!!, R.string.difficulty_save_toast, Toast.LENGTH_SHORT).show()

            dismiss()
        }

        binding.multiplication.let { binding ->
            binding.operationTitle = getString(R.string.difficulty_operation_multiplication)
            binding.allowAdjustSecondOperandSize = true

            binding.enableView.listener = object: EnableOperatorListener {
                override fun updateEnableOperator(enable: Boolean) {
                    difficulty = difficulty.copy(
                            multiplication = difficulty.multiplication.copy(
                                    enable = enable
                            )
                    )

                    updateAll() // because it can change if an other operation can be disabled
                }
            }

            binding.tenTransgressionSwitch.visibility = View.GONE
            binding.negativeResultSwitch.visibility = View.GONE

            binding.digits1.setOnChangeListener {
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            multiplication = difficulty.multiplication.copy(
                                    digitsOfFirstNumber = it + 1
                            )
                    )

                    updateMultiplication()
                }
            }

            binding.digits2.setOnChangeListener {
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            multiplication = difficulty.multiplication.copy(
                                    digitsOfSecondNumber = it + 1
                            )
                    )

                    updateMultiplication()
                }
            }
        }

        binding.addition.let { binding ->
            binding.operationTitle = getString(R.string.difficulty_operation_addition)

            binding.enableView.listener = object: EnableOperatorListener {
                override fun updateEnableOperator(enable: Boolean) {
                    difficulty = difficulty.copy(
                            addition = difficulty.addition.copy(
                                    enable = enable
                            )
                    )

                    updateAll() // because it can change if an other operation can be disabled
                }
            }

            binding.tenTransgressionSwitch.setOnCheckedChangeListener { _, enable ->
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            addition = difficulty.addition.copy(
                                    enableTenTransgression = enable,
                                    digitsOfSecondNumber = if (enable) difficulty.addition.digitsOfSecondNumber else 1
                            )
                    )

                    updateAddition()
                }
            }

            binding.negativeResultSwitch.visibility = View.GONE

            binding.digits1.setOnChangeListener {
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            addition = difficulty.addition.copy(
                                    digitsOfFirstNumber = it + 1
                            )
                    )

                    updateAddition()
                }
            }

            binding.digits2.setOnChangeListener {
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            addition = difficulty.addition.copy(
                                    digitsOfSecondNumber = it + 1
                            )
                    )

                    updateAddition()
                }
            }
        }

        binding.subtraction.let { binding ->
            binding.operationTitle = getString(R.string.difficulty_operation_subtraction)

            binding.enableView.listener = object: EnableOperatorListener {
                override fun updateEnableOperator(enable: Boolean) {
                    difficulty = difficulty.copy(
                            subtraction = difficulty.subtraction.copy(
                                    enable = enable
                            )
                    )

                    updateAll() // because it can change if an other operation can be disabled
                }
            }

            binding.tenTransgressionSwitch.setOnCheckedChangeListener { _, enable ->
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            subtraction = difficulty.subtraction.copy(
                                    enableTenTransgression = enable,
                                    digitsOfSecondNumber = if (enable) difficulty.subtraction.digitsOfSecondNumber else 1,
                                    allowNegativeResults = enable && difficulty.subtraction.allowNegativeResults
                            )
                    )

                    updateSubtraction()
                }
            }

            binding.negativeResultSwitch.setOnCheckedChangeListener { _, enable ->
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            subtraction = difficulty.subtraction.copy(
                                    allowNegativeResults = enable,
                                    digitsOfSecondNumber = if (enable)
                                        difficulty.subtraction.digitsOfSecondNumber
                                    else
                                        difficulty.subtraction.digitsOfSecondNumber.coerceAtMost(difficulty.subtraction.digitsOfFirstNumber)
                            )
                    )

                    updateSubtraction()
                }
            }

            binding.digits1.setOnChangeListener {
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            subtraction = difficulty.subtraction.copy(
                                    digitsOfFirstNumber = it + 1,
                                    allowNegativeResults = difficulty.subtraction.allowNegativeResults || difficulty.subtraction.digitsOfSecondNumber > it + 1
                            )
                    )

                    updateSubtraction()
                }
            }

            binding.digits2.setOnChangeListener {
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            subtraction = difficulty.subtraction.copy(
                                    digitsOfSecondNumber = it + 1,
                                    allowNegativeResults = difficulty.subtraction.allowNegativeResults || it + 1 > difficulty.subtraction.digitsOfFirstNumber
                            )
                    )

                    updateSubtraction()
                }
            }
        }

        binding.factorization.let { binding ->
            binding.enableView.listener = object: EnableOperatorListener {
                override fun updateEnableOperator(enable: Boolean) {
                    difficulty = difficulty.copy(
                            factorization = difficulty.factorization.copy(
                                    enable = enable
                            )
                    )

                    updateAll() // because it can change if an other operation can be disabled
                }
            }

            binding.smallestNumber.setOnValueChangedListener { _, _, newValue ->
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            factorization = difficulty.factorization.copy(
                                    smallestNumber = newValue
                            )
                    )

                    updateFactorization()
                }
            }

            binding.biggestNumber.setOnValueChangedListener { _, _, newValue ->
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            factorization = difficulty.factorization.copy(
                                    biggestNumber = newValue
                            )
                    )

                    updateFactorization()
                }
            }
        }

        binding.division.let { binding ->
            binding.enableView.listener = object: EnableOperatorListener {
                override fun updateEnableOperator(enable: Boolean) {
                    difficulty = difficulty.copy(
                            division = difficulty.division.copy(
                                    enable = enable
                            )
                    )

                    updateAll() // because it can change if an other operation can be disabled
                }
            }

            binding.allowRemainderSwitch.setOnCheckedChangeListener { _, isChecked ->
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            division = difficulty.division.copy(
                                    withRemainder = isChecked
                            )
                    )

                    updateDivision()
                }
            }

            binding.dividendBar.max = DivisionConfig.MAX_DIVIDEND_SUGGESTIONS.size - 1
            binding.divisorBar.max = DivisionConfig.MAX_DIVISOR_SUGGESTIONS.size - 1

            binding.dividendBar.setOnChangeListener { newValue ->
                if (!isUpdatingValues) {
                    val newDividend = DivisionConfig.MAX_DIVIDEND_SUGGESTIONS[newValue]

                    difficulty = difficulty.copy(
                            division = difficulty.division.copy(
                                    maxDividend = newDividend,
                                    maxDivisor = difficulty.division.maxDivisor.coerceAtMost(newDividend)
                            )
                    )

                    updateDivision()
                }
            }

            binding.divisorBar.setOnChangeListener { newValue ->
                if (!isUpdatingValues) {
                    val newDivisor = DivisionConfig.MAX_DIVISOR_SUGGESTIONS[newValue]

                    difficulty = difficulty.copy(
                            division = difficulty.division.copy(
                                    maxDivisor = newDivisor,
                                    maxDividend = difficulty.division.maxDividend.coerceAtLeast(newDivisor)
                            )
                    )

                    updateDivision()
                }
            }
        }

        binding.profile.apply {
            easy.setOnClickListener {
                difficulty = TaskDifficultyProfiles.easy

                binding.flipper.openParentScreen(PAGE_OVERVIEW)
                updateAll()
            }

            medium.setOnClickListener {
                difficulty = TaskDifficultyProfiles.medium

                binding.flipper.openParentScreen(PAGE_OVERVIEW)
                updateAll()
            }

            hard.setOnClickListener {
                difficulty = TaskDifficultyProfiles.hard

                binding.flipper.openParentScreen(PAGE_OVERVIEW)
                updateAll()
            }
        }

        binding.general.let { binding ->
            binding.tasksPerRound.setOnChangeListener {
                if (!isUpdatingValues) {
                    val old = difficulty.roundConfig
                    val new = (it + 1) * 5

                    difficulty = difficulty.copy(
                            roundConfig = difficulty.roundConfig.copy(
                                    tasksPerRound = new,
                                    maximalTimePerRound = (old.maximalTimePerRound * ((it + 1) * 5 ) / old.tasksPerRound),
                                    maximalMistakesPerRound = old.maximalMistakesPerRound.coerceAtMost(new),
                                    reAskRightInNextRound = old.reAskRightInNextRound.coerceAtMost(new),
                                    reAskWrongInNextRound = old.reAskWrongInNextRound.coerceAtMost(new)
                            )
                    )

                    updateGeneral()
                }
            }

            binding.timePerRound.setOnChangeListener {
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            roundConfig = difficulty.roundConfig.copy(
                                    maximalTimePerRound = (it + 1) * (difficulty.roundConfig.tasksPerRound * 1000)
                            )
                    )

                    updateGeneral()
                }
            }

            binding.takeRightTasks.setOnChangeListener {
                if (!isUpdatingValues) {
                    val old = difficulty.roundConfig
                    val new = it

                    difficulty = difficulty.copy(
                            roundConfig = old.copy(
                                    reAskRightInNextRound = new
                            )
                    )

                    updateGeneral()
                }
            }

            binding.takeWrongTasks.setOnChangeListener {
                if (!isUpdatingValues) {
                    val old = difficulty.roundConfig
                    val new = it

                    difficulty = difficulty.copy(
                            roundConfig = old.copy(
                                    reAskWrongInNextRound = new
                            )
                    )

                    updateGeneral()
                }
            }

            binding.maxWrongTasks.setOnChangeListener {
                if (!isUpdatingValues) {
                    val old = difficulty.roundConfig
                    val new = it

                    difficulty = difficulty.copy(
                            roundConfig = old.copy(
                                    maximalMistakesPerRound = new,
                                    reAskWrongInNextRound = old.reAskWrongInNextRound.coerceAtMost(new)
                            )
                    )

                    updateGeneral()
                }
            }
        }

        // enabled until the view state was restored => see below
        isUpdatingValues = true

        return binding.root
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        isUpdatingValues = false

        // eventually restore values which were modified by the view restoration
        if (didLoadDifficulty) {
            updateAll()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        if (didLoadDifficulty) {
            outState.putString(STATE_DIFFICULTY, writeJson { difficulty.serialize(it) })
            outState.putInt(STATE_SCREEN, binding.flipper.displayedChild)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = object: BottomSheetDialog(context!!, theme) {
        override fun onBackPressed() {
            if (binding.flipper.displayedChild != PAGE_OVERVIEW && binding.flipper.displayedChild != PAGE_LOADING) {
                updateOverview()

                binding.flipper.openParentScreen(PAGE_OVERVIEW)
            } else {
                super.onBackPressed()
            }
        }
    }

    fun show(fragmentManager: FragmentManager) = show(fragmentManager, DIALOG_TAG)
}