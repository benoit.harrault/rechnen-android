/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */

package com.rechnen.app.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import androidx.gridlayout.widget.GridLayout
import com.rechnen.app.R
import com.rechnen.app.data.modelparts.InputConfiguration

class KeyboardView(context: Context, attributeSet: AttributeSet): FrameLayout(context, attributeSet) {
    var listener: Listener? = null

    var type: InputConfiguration.KeyboardType = InputConfiguration.KeyboardType.Phone
        get() = field
        set(value) {
            if (value != field) {
                field = value
                updateLayoutParamsForButtonSorting()
            }
        }

    var confirmButtonLocation: InputConfiguration.ConfirmButtonLocation = InputConfiguration.ConfirmButtonLocation.Right
        get() = field
        set(value) {
            if (value != field) {
                field = value
                updateLayoutParamsForConfirmButtonLocation()
            }
        }

    var scaleFactor: Int = 0
        get() = field
        set(value) {
            if (value < 0 || value > 100) throw IllegalArgumentException()

            if (value != field) {
                field = value
                requestLayout()
            }
        }

    init {
        inflate(context, R.layout.training_keyboard_view, this)
    }

    private val keyboard = getChildAt(0)

    private val digitButtons = arrayOf(
            R.id.button0,
            R.id.button1,
            R.id.button2,
            R.id.button3,
            R.id.button4,
            R.id.button5,
            R.id.button6,
            R.id.button7,
            R.id.button8,
            R.id.button9
    ).map { findViewById<Button>(it) }

    private val okButton = findViewById<Button>(R.id.ok_button)
    private val specialButton = findViewById<Button>(R.id.special_button)

    var specialButtonLabel: String?
        get() = specialButton.text.toString()
        set(value) {
            specialButton.text = value
            specialButton.isEnabled = isEnabled && !value.isNullOrBlank()
        }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)

        digitButtons.forEach { it.isEnabled = enabled }
        specialButton.isEnabled = isEnabled && !specialButtonLabel.isNullOrBlank()
        okButton.isEnabled = isEnabled
    }

    private fun applyLocation(view: View, column: Int, row: Int) {
        (view.layoutParams as GridLayout.LayoutParams).let { params ->
            params.rowSpec = GridLayout.spec(row, 1.0f)
            params.columnSpec = GridLayout.spec(column, 1.0f)
        }
    }

    private fun updateLayoutParamsForButtonSorting() {
        for (i in 1..9) {
            val col = (i - 1) % 3
            val rowBase = (i - 1) / 3
            val row = when (type) {
                InputConfiguration.KeyboardType.Phone -> rowBase
                InputConfiguration.KeyboardType.Calculator -> 2 - rowBase
            }

            applyLocation(view = digitButtons[i], column = col, row = row)
        }
    }

    private fun updateLayoutParamsForConfirmButtonLocation() {
        applyLocation(
                view = okButton,
                column = when (confirmButtonLocation) {
                    InputConfiguration.ConfirmButtonLocation.Right -> 2
                    InputConfiguration.ConfirmButtonLocation.Left -> 0
                },
                row = 3
        )

        applyLocation(
                view = specialButton,
                column = when (confirmButtonLocation) {
                    InputConfiguration.ConfirmButtonLocation.Right -> 0
                    InputConfiguration.ConfirmButtonLocation.Left -> 2
                },
                row = 3
        )
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        keyboard.measure(0, 0)

        val keyboardWidth = keyboard.measuredWidth
        val keyboardHeight = keyboard.measuredHeight

        val isWidthAtMost = MeasureSpec.getMode(widthMeasureSpec) == MeasureSpec.AT_MOST
        val isHeightAtMost = MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.AT_MOST

        if (isWidthAtMost && isHeightAtMost) {
            val maxWidth = MeasureSpec.getSize(widthMeasureSpec)
            val maxHeight = MeasureSpec.getSize(heightMeasureSpec)

            val maxWidthFactor = maxWidth * 100 / keyboardWidth
            val maxHeightFactor = maxHeight * 100 / keyboardHeight
            val maxFactor = maxWidthFactor.coerceAtMost(maxHeightFactor)

            val usedScaledFactor = if (maxFactor > 100) 100 + scaleFactor * (maxFactor - 100) / 100 else 100

            setMeasuredDimension(keyboardWidth * usedScaledFactor / 100, keyboardHeight * usedScaledFactor / 100)
        } else if (isWidthAtMost) {
            val targetHeight = MeasureSpec.getSize(heightMeasureSpec)
            val usedScaleFactor = targetHeight * 100 / keyboardHeight

            setMeasuredDimension(
                    MeasureSpec.makeMeasureSpec(keyboardWidth * usedScaleFactor / 100, MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(targetHeight, MeasureSpec.EXACTLY)
            )
        } else if (isHeightAtMost) {
            val targetWidth = MeasureSpec.getSize(widthMeasureSpec)
            val usedScaleFactor = targetWidth * 100 / keyboardWidth

            setMeasuredDimension(
                    MeasureSpec.makeMeasureSpec(targetWidth, MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(keyboardHeight * usedScaleFactor / 100, MeasureSpec.EXACTLY)
            )
        } else {
            setMeasuredDimension(widthMeasureSpec, heightMeasureSpec)
        }

        if (MeasureSpec.getMode(measuredHeightAndState) == MeasureSpec.EXACTLY && MeasureSpec.getMode(measuredWidthAndState) == MeasureSpec.EXACTLY) {
            keyboard.measure(measuredWidthAndState, measuredHeightAndState)
        }
    }

    init {
        okButton.setOnClickListener { listener?.onInputConfirmed() }
        specialButton.setOnClickListener { listener?.onSpecialClicked() }
        digitButtons.forEachIndexed { index, button -> button.setOnClickListener { listener?.onDigitEntered(index) } }

        specialButtonLabel = null

        updateLayoutParamsForButtonSorting()
        updateLayoutParamsForConfirmButtonLocation()
    }

    interface Listener {
        fun onDigitEntered(digit: Int)
        fun onSpecialClicked()
        fun onInputConfirmed()
    }
}