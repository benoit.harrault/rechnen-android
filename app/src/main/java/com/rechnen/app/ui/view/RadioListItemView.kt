/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.view

import android.content.Context
import android.util.AttributeSet
import android.widget.Checkable
import android.widget.CheckedTextView
import android.widget.FrameLayout
import com.rechnen.app.R

class RadioListItemView(context: Context, attributeSet: AttributeSet): FrameLayout(context, attributeSet), Checkable {
    init {
        inflate(context, R.layout.radio_list_item_view, this)
    }

    private val textView = getChildAt(0) as CheckedTextView

    override fun isChecked(): Boolean = textView.isChecked
    override fun setChecked(checked: Boolean) { textView.isChecked = checked }
    override fun toggle() = textView.toggle()

    var radioLabel: String?
        get() = textView.text.toString()
        set(value) { textView.text = value }

    init {
        val attributes = context.obtainStyledAttributes(attributeSet, R.styleable.RadioListItemView)

        radioLabel = attributes.getString(R.styleable.RadioListItemView_radioLabel)

        attributes.recycle()
    }
}