/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.tasktype

import com.rechnen.app.R
import com.rechnen.app.data.modelparts.TaskType

data class TaskTypeInfo(val labelResourceId: Int) {
    companion object {
        private val ADDITION = TaskTypeInfo(
                labelResourceId = R.string.difficulty_operation_addition
        )

        private val SUBTRACTION = TaskTypeInfo(
                labelResourceId = R.string.difficulty_operation_subtraction
        )

        private val MULTIPLICATION = TaskTypeInfo(
                labelResourceId = R.string.difficulty_operation_multiplication
        )

        private val FACTORIZATION = TaskTypeInfo(
                labelResourceId = R.string.difficulty_operation_factorization
        )

        private val DIVISION = TaskTypeInfo(
                labelResourceId = R.string.difficulty_operation_division
        )

        fun get(type: TaskType): TaskTypeInfo = when (type) {
            TaskType.Addition -> ADDITION
            TaskType.Subtraction -> SUBTRACTION
            TaskType.Multiplication -> MULTIPLICATION
            TaskType.Factorization -> FACTORIZATION
            TaskType.Division -> DIVISION
        }
    }
}