/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.training

import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.util.Log
import com.rechnen.app.BuildConfig
import com.rechnen.app.data.Database
import java.util.concurrent.Executors

class PlaytimeSaver (val userId: Int, val database: Database) {
    companion object {
        private const val LOG_TAG = "PlaytimeSaver"
        private const val MAX_TIME_WITHOUT_START = 1000 * 30L
        private const val MAX_TIME_TO_QUEUE = 1000 * 60L

        private val handler = Handler(Looper.getMainLooper())
        private val executor = Executors.newSingleThreadExecutor()

        private fun uptime() = SystemClock.uptimeMillis()
    }

    private var timeToAdd = 0L
    private var lastStartUptime = 0L

    fun startOrContinue() {
        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, "startOrContinue()")
        }

        val uptime = uptime()

        handler.post {
            handleTimeSinceLastStart(uptime)
            lastStartUptime = uptime

            if (timeToAdd > MAX_TIME_TO_QUEUE) {
                commitAsync()
            }
        }
    }

    fun stop() {
        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, "stop()")
        }

        val uptime = uptime()

        handler.post {
            handleTimeSinceLastStart(uptime)
            commitAsync()
        }
    }

    private fun handleTimeSinceLastStart(uptime: Long) {
        val lastStart = lastStartUptime

        if (lastStart == 0L) return

        timeToAdd += (uptime - lastStart).coerceAtLeast(0).coerceAtMost(MAX_TIME_WITHOUT_START)
        lastStartUptime = 0
    }

    private fun commitAsync() {
        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, "commitAsync()")
        }

        val thisTimeToAdd = timeToAdd; timeToAdd = 0

        if (thisTimeToAdd > 0) {
            if (BuildConfig.DEBUG) {
                Log.d(LOG_TAG, "add $thisTimeToAdd for $userId")
            }

            executor.submit {
                database.user().increaseUserPlayTimeSync(userId, thisTimeToAdd)
            }
        }
    }
}